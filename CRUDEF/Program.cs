﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ExemploCRUD
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ExemploContext())
            {
                Pessoa pessoa = new Pessoa();
                pessoa.Nome = "Marcia";

                Endereco endereco = new Endereco();
                endereco.Rua = "Mal. Deodoro, 300";
                endereco.Bairro = "Centro";

                pessoa.Enderecos.Add(endereco);

                db.Pessoas.Add(pessoa);
                db.SaveChanges();

                List<Pessoa> pessoas = db.Pessoas.ToList<Pessoa>();
 
                //var query = from p in db.Pessoas
                //            orderby p.Nome
                //            select p;

                Console.WriteLine("Todos as pessoas no banco de dados:");
                foreach (var item in pessoas)
                {
                    Console.WriteLine(item.Nome);
                }

                Console.WriteLine("Pressione uma tecla para sair...");
                Console.ReadKey();
            }
        }
    }
}