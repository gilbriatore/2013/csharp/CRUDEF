﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExemploCRUD
{
    public class Endereco
    {
        public int EnderecoId { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }

        public int PessoaId { get; set; }
        public Pessoa Pessoa { get; set; }
    }
}
