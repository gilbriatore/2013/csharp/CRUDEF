﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExemploCRUD
{
    public class Pessoa
    {
        public int PessoaId { get; set; }
        public string Nome { get; set; }

        private List<Endereco> enderecos;

        public Pessoa()
        {
            this.enderecos = new List<Endereco>();
        }

        public List<Endereco> Enderecos
        {
            get { return enderecos; ; }
            set { enderecos = value; }
        }
    }
}
