﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace ExemploCRUD
{
    public class ExemploContext : DbContext
    {
        public ExemploContext()
            : base("ConexaoExemploCRUDEF")
        {
        }
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
    }
}
